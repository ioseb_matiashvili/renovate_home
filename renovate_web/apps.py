from django.apps import AppConfig


class RenovateWebConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'renovate_web'
