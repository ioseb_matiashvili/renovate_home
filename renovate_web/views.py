from django.db import models
from django.http import HttpResponse
from django.views import View
from django.shortcuts import render

# Create your models here.


class StartinPageView(View):
    # template_name = "renovate_web/index.html"

    def get(self, request):
        # context = {}
        return render(request, "renovate_web/index.html", {})
