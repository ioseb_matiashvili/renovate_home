from django.urls import path
from . import views

urlpatterns = [
    path('', views.StartinPageView.as_view(), name='starting-page')
]
